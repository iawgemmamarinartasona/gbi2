<?php

namespace App\GraphQL\Mutations;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class Login
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        if (!Auth::attempt($args)){
            return null;
        }

        $user = USer::where('email', auth()->user()->email)->firstOrFail();

        if($user->isAdmin == "true" ){
            $token = $user->createToken('token', ['admin'])->plainTextToken;
        
        }else{
            $token = $user->createToken('token')->plainTextToken;
        }
        $user['token'] = $token;

        return $user;
    }
}
