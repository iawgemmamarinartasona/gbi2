<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class UserGraph
{

    public function updateUser($_, array $args) : User {
        $info = Arr::only($args, [
            'name', 'lastName', 'password'
        ]);
        $id = Auth::id();
        $user = User::where("id", $id);
        $user->update($info);

        return $user->first();
    }
}